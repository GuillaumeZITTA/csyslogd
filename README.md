# CSyslogD : tiny syslogd for container usage.

# Status: Beta

> Beta it may not work as expected

> behaviour may change

> parameters may changes

## Goals

Goals of csyslogd are :
* Ease log collection through widely supported protocol (RFC3164 and RFC5424 syslog messages)
  * Many software used in containers support natively logging to syslog
* Ease log transport to structured log infrastructure
  * Do minimal message parsing locally
* Easy integration to container images and be lightweiht.
  * Onefile, near no dependency
    * depends on Python 3, but maybe provided as binary one day.
  * minimal set of transport protocols
    * may be added in the future 
      * JSON over TCP
      * relay to another syslog server
      * TLS

## Non-Goals

csyslogd does not replace tools like rsyslog, syslog-ng, logstash ...

# Setup

## Requirements

* Python3

## in Dockerfile

```Dockerfile
ADD https://gitlab.com/GuillaumeZITTA/csyslogd/-/raw/master/csyslogd/csyslogd_cmd.py /usr/local/bin/csyslogd

# Usually we dont keep language like .py extension in 'bin' dirs
RUN chmod +x /usr/local/bin/csyslogd
```

# Usage

## in entrypoint or cmd script :

```bash
/usr/local/bin/csyslogd &
```

## Environment variables

* CSD_CONSOLE_PATH
  * Consolt output of logs.
  * Default value: `/proc/1/fd/1` (stdout of main container process: pid 1)
* CSD_INPUT_SOCKET_PATH
  * Input socket: Socket from which logs (RFC5424 or RFC3164) are collected
  * Default value: `/dev/log` (usual path of syslogd socket)
* CSD_MAX_MSG_SIZE
  * Maximum Message size: extra bytes will be cut
  * Default: 64KB
* CSD_JOU_DEST
  * Enable Json Over Udp. Each log line will be converted to structured data and sent 
    to provided host and port.
  * Format: `host:port`
  * Default: disabled

# JSON over UDP setup

> @TODO: give example of logstash setup

# Contribution

Fork and make a merge request. But keep in mind that it needs to stay simple and will not replace a real syslog daemon.