FROM alpine:3.12

RUN apk add python3 util-linux

COPY csyslogd/csyslogd_cmd.py /usr/local/bin/csyslogd

COPY test_docker/entrypoint.sh /usr/local/bin/entrypoint.sh

RUN chmod +x /usr/local/bin/*

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]