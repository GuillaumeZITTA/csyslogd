from unittest import TestCase
from unittest.mock import patch
from csyslogd.csyslogd_cmd import CSyslogDCmd


class CSyslogDTest(TestCase):
    def setUp(self) -> None:
        self.maxDiff = None

    @patch.dict(
        "os.environ",
        {"CSD_CONSOLE_PATH": "/dev/null", "CSD_INPUT_SOCKET_PATH": ".dev_log"},
    )
    def test_parse_rfc3156(self):
        csyslogd = CSyslogDCmd()
        log_line = r"<34>Oct 11 22:14:15 mymachine su: 'su root' failed for lonvick on /dev/pts/8"
        dict_data = csyslogd.parse_pri(log_line)
        self.assertDictEqual(
            {
                "facility": "auth",
                "message_format": "RFC3164",
                "raw_message": "Oct 11 22:14:15 mymachine su: 'su root' failed for lonvick on "
                "/dev/pts/8",
                "severity": "crit",
            },
            dict_data,
        )
        csyslogd.parse_all(dict_data)
        self.assertDictEqual(
            {
                "time": "Oct 11 22:14:15",
                "host": "mymachine",
                "ident": "su",
                "pid": None,
                "message": "'su root' failed for lonvick on /dev/pts/8",
                "facility": "auth",
                "severity": "crit",
            },
            dict_data,
        )

    @patch.dict(
        "os.environ",
        {"CSD_CONSOLE_PATH": "/dev/null", "CSD_INPUT_SOCKET_PATH": ".dev_log"},
    )
    def test_parse_rfc5424(self):
        csyslogd = CSyslogDCmd()
        log_line = '<165>1 2003-10-11T22:14:15.003Z mymachine.example.com evntslog - ID47 [exampleSDID@32473 iut="3" eventSource="Application" eventID="1011"][examplePriority@32473 class="high"] BOMAn application event log entry...'
        dict_data = csyslogd.parse_pri(log_line)
        self.assertDictEqual(
            {
                "facility": "local4",
                "message_format": "RFC5424",
                "raw_message": "1 2003-10-11T22:14:15.003Z mymachine.example.com evntslog - "
                'ID47 [exampleSDID@32473 iut="3" eventSource="Application" '
                'eventID="1011"][examplePriority@32473 class="high"] BOMAn '
                "application event log entry...",
                "severity": "notice",
            },
            dict_data,
        )

        csyslogd.parse_all(dict_data)

        self.assertDictEqual(
            {
                "time": "2003-10-11T22:14:15.003Z",
                "host": "mymachine.example.com",
                "ident": "evntslog",
                "pid": "-",
                "msgid": "ID47",
                "message": "BOMAn application event log entry...",
                "facility": "local4",
                "severity": "notice",
                "extradata": '[exampleSDID@32473 iut="3" eventSource="Application" eventID="1011"][examplePriority@32473 class="high"]',
            },
            dict_data,
        )

    @patch.dict(
        "os.environ",
        {"CSD_CONSOLE_PATH": ".out", "CSD_INPUT_SOCKET_PATH": ".dev_log"},
    )
    def test_run_line(self):
        csyslogd = CSyslogDCmd()
        log_line = r"<34>Oct 11 22:14:15 mymachine su: 'su root' failed for lonvick on /dev/pts/8"
        csyslogd.run_line(log_line)