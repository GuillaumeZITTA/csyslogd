#!/usr/bin/env python3

"""csyslogd: A tiny syslogd designed to ease log collection on containers.

The goal is to have something working in one file.

This file is and should stay independent from any non-standard python module.

See README.md for usage and environment variables
"""

import json
import os
import re
import socket

MYPY = False
if MYPY:
    # pylint: disable=unused-import
    from typing import Any, Dict

HOST_PORT_FORMAT = re.compile(r"(?P<host>[^:]+):(?P<port>[0-9]+)")

# Syslog patterns
SYSLOG_PRI = re.compile(r"\A\<(?P<pri>[0-9]{1,3})\>(?P<after_pri>.*)$")

AFTER_PRI_FORMAT = {
    "RFC3164": re.compile(
        r"^(?P<time>[^ ]* {1,2}[^ ]* [^ ]*)"
        r" (?P<host>[^ ]*) (?P<ident>[^ :\[]*)(?:\[(?P<pid>[0-9]+)\])?(?:[^\:]*\:)?"
        r" *(?P<message>.*)$"
    ),
    "RFC5424": re.compile(
        r"^[1-9]\d{0,2} (?P<time>[^ ]+)"
        r" (?P<host>[!-~]{1,255}) (?P<ident>[!-~]{1,48})"
        r" (?P<pid>[!-~]{1,128}) (?P<msgid>[!-~]{1,32})"
        r" (?P<extradata>(?:\-|(?:\[.*?(?<!\\)\])+))(?: (?P<message>.+))?$"
    ),
}
SEVERITY = [
    "emerg",
    "alert",
    "crit",
    "error",
    "warn",
    "notice",
    "info",
    "debug",
]

FACILITY = [
    "kern",
    "user",
    "mail",
    "daemon",
    "auth",
    "syslog",
    "lpr",
    "news",
    "uucp",
    "cron",
    "authpriv",
    "ftp",
    "ntp",
    "audit",
    "alert",
    "clockdaemon",
    "local0",
    "local1",
    "local2",
    "local3",
    "local4",
    "local5",
    "local6",
    "local7",
]


class CSyslogDCmd:
    def __init__(self) -> None:
        # Console Output
        # Default: stdout of PID 1 process
        console_path = os.environ.get("CSD_CONSOLE_PATH", "/proc/1/fd/1")
        self.console = open(console_path, "wb")
        self.console_print("csyslogd: starting")

        # Input Socket
        # Default: /dev/log like any other syslogd
        input_socket_path = os.environ.get("CSD_INPUT_SOCKET_PATH", "/dev/log")
        if os.path.exists(input_socket_path):
            self.console_print(f"csyslogd: removing existing {input_socket_path}")
            os.remove(input_socket_path)

        self.input_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
        self.input_socket.bind(input_socket_path)

        os.chmod(input_socket_path, 0o666)
        self.console_print(f"csyslogd listening on {input_socket_path}")

        # Max Message size (actually the buffer size while reading the socket)
        # Default: 64KB
        self.buffer_size = int(os.environ.get("CSD_MAX_MSG_SIZE", "65535"))

        # Optionnally init UDP send
        send_udp_json_target = os.environ.get("CSD_JOU_DEST")
        if send_udp_json_target is not None:
            match = HOST_PORT_FORMAT.search(send_udp_json_target)
            if match:
                self.udp_send_address = (match.group("host"), int(match.group("port")))
                self.udp_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            else:
                raise Exception(
                    "found CSD_JOU_DEST env var but unable to parse host:port"
                )
        else:
            self.udp_send_address = None
            self.udp_sock = None

    def run(self):
        """Execute the main loop."""
        while True:
            try:
                bytes_data = self.input_socket.recv(self.buffer_size)
                self.run_line(bytes_data.decode())
            except Exception as exception:
                self.console_print(f"DSD: {exception}")

    def console_print(self, string: str) -> None:
        """Print to console.

        Args:
            string (str): String to push
        """
        self.console.write(string.encode() + b"\n")
        self.console.flush()

    def push_json_to_udp(self, fields: "Dict[str, Any]"):
        """Send a json over UDP with the message

        Args:
            fields (Dict[str, Any]): Dictionary version of log message
        """
        json_message = json.dumps(fields)
        self.udp_sock.sendto(json_message.encode(), self.udp_send_address)

    def run_line(self, log_line: str):
        """Execute a single line.

        Args:
            log_line (str): log line.
        """
        log_fields = self.parse_pri(log_line)

        self.console_print("{facility}.{severity} {raw_message}".format(**log_fields))

        if self.udp_sock is not None:
            self.parse_all(log_fields)
            self.push_json_to_udp(log_fields)
            self.console_print(str(log_fields))

    def parse_pri(self, syslog_line: str) -> "Dict[str,Any]":
        """Decode PRI.

        Returns:
            Dict[str, Any]: Structured facility, severity and raw message after PRI
        """
        pri_match = SYSLOG_PRI.search(syslog_line)
        if not pri_match:
            # this does seems to be a log line
            raise Exception(f"Unparseable (no pri) line: {syslog_line}")

        # Manage PRIORITY
        pri = int(pri_match.group("pri"))

        # Manage rest of the line
        raw_message = pri_match.group("after_pri")

        if raw_message.startswith("1"):
            message_format = "RFC5424"
        else:
            message_format = "RFC3164"

        return dict(
            facility=FACILITY[pri // 8],
            severity=SEVERITY[pri % 8],
            raw_message=raw_message,
            message_format=message_format,
        )

    def parse_all(self, fields: "Dict[str,Any]") -> None:
        """Completly convert log into structured data.

        Args:
            fields (Dict[str,Any]): pre_structured, will be updated
        """
        after_pri_re = AFTER_PRI_FORMAT[fields["message_format"]]  # type: re.Pattern
        match = after_pri_re.search(fields["raw_message"])
        if not match:
            fields["parsing_error"] = f"does not match {fields['message_format']}"
            return

        fields.update(match.groupdict())
        fields.pop("raw_message")
        fields.pop("message_format")


if __name__ == "__main__":
    CSyslogDCmd().run()
